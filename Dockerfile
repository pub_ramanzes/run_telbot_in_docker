FROM python:latest


# Install any necessary dependencies
RUN pip install telebot
RUN pip install git+https://github.com/Uberi/speech_recognition.git
RUN pip install pydub
RUN pip install requests

RUN apt update
RUN apt upgrade -y
RUN apt install -y ffmpeg


# Copy your python script to the working directory
COPY audio_to_text.py .




# Set the entrypoint to run your python script
ENTRYPOINT ["python", "audio_to_text.py"]

# Restart the container if it stops
CMD ["--restart=always"]

