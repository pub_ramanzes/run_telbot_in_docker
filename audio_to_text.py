import os
import telebot
import speech_recognition
from pydub import AudioSegment
import requests

# Ниже нужно вставить токен, который дал BotFather при регистрации
# Пример: token = '2007628239:AAEF4ZVqLiRKG7j49EC4vaRwXjJ6DN6xng8'
#token = '6159516707:AAF3uQfAOHv2eKdvVwvlbV2dnqHU_i1tHPo'  # <<< Ваш токен
#idid_botimport os

#При создании контейнера нужно передавать в параметрах контейнеру Docker, используя переменные среды.
#docker run -e TOKEN='ваш токен вашего бота полученного у @botfather в телеграмм' my_image


token = os.environ['TOKEN']



bot = telebot.TeleBot(token)

#print(requests.get('https://api.telegram.org/bot{token}'))
welcome_message = 'Привет, если ты отправишь сюда голосовое, я переведу его в текст и выведу в этот чат индивидуально. Чем короче аудио сообщение, тем быстрее я это сделаю. Для длинных сообщений придётся немного подождать.'

# Define the menu items
#menu_items = ["/start", "Option 2", "Option 3"]

        # Prompt the user to select a menu item
#print("Please select a menu option:")
#for i, item in enumerate(menu_items):
#    print(f"{i + 1}. {item}")
#    choice = input("Enter your choice (1-3): ")


#Welcome to my bot! Here are some things you can do:
#- /option1: Description of option 1
#- /option2: Description of option 2
#'''

# Define your options and their respective descriptions
#option1_description = 'This is the description for option 1'
#option2_description = 'This is the description for option 2'

# Add a handler for the /start command
@bot.message_handler(commands=['start'])
def send_welcome(message):
    # Send the welcome message along with the menu options
    bot.reply_to(message, welcome_message)


def oga2wav(filename):
    # Конвертация формата файлов
    new_filename = filename.replace('.oga', '.wav')
    audio = AudioSegment.from_file(filename)
    audio.export(new_filename, format='wav')
    return new_filename


def recognize_speech(oga_filename):
    # Перевод голоса в текст + удаление использованных файлов
    wav_filename = oga2wav(oga_filename)
    recognizer = speech_recognition.Recognizer()

    with speech_recognition.WavFile(wav_filename) as source:
        wav_audio = recognizer.record(source)

    text = recognizer.recognize_google(wav_audio, language='ru')

    if os.path.exists(oga_filename):
        os.remove(oga_filename)

    if os.path.exists(wav_filename):
        os.remove(wav_filename)

    return text


def download_file(bot, file_id):
    # Скачивание файла, который прислал пользователь
    file_info = bot.get_file(file_id)
    downloaded_file = bot.download_file(file_info.file_path)
    filename = file_id + file_info.file_path
    filename = filename.replace('/', '_')
    with open(filename, 'wb') as f:
        f.write(downloaded_file)
    return filename


#@bot.message_handler(commands=['start'])
#def say_hi(message):
    # Функция, отправляющая "Привет" в ответ на команду /start
 #   bot.send_message(message.chat.id, 'Привет')


@bot.message_handler(content_types=['voice'])
def transcript(message):
    # Функция, отправляющая текст в ответ на голосовое
    filename = download_file(bot, message.voice.file_id)
    text = recognize_speech(filename)
    bot.send_message(message.chat.id, text)


# Запускаем бота. Он будет работать до тех пор, пока работает ячейка (крутится значок слева).
# Остановим ячейку - остановится бот
bot.polling()
