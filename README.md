
заходим на свой сервер, который работает круглые сутки

устанавливаем в свою ос docker

чтобы не запускать контейнер от рута.
#Create the "docker" group (if it doesn't already exist):
sudo groupadd docker
#добавляем пользователя в группу
#Add your user account to the "docker" group:
sudo usermod -aG docker $USER


#создаём свой образ по Dockerfile
docker build -t name_yours_image .


#запускаем контейнер в фоновом режиме, указывая токен своего бота в переменную среды TOKEN

docker run -d -e TOKEN='ваш токен' --restart=always name_yours_image
